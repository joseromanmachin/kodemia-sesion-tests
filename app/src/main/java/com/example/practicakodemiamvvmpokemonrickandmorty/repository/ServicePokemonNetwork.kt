package com.example.practicakodemiamvvmpokemonrickandmorty.repository

import com.example.practicakodemiamvvmpokemonrickandmorty.core.ApiPokemon
import com.example.practicakodemiamvvmpokemonrickandmorty.core.RetrofitInstance
import com.example.practicakodemiamvvmpokemonrickandmorty.data.models.PokemonResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class ServicePokemonNetwork {

    val retrofit = RetrofitInstance.getRetrofit().create(ApiPokemon::class.java)

    suspend fun obtenerPokemones():Response<PokemonResponse>{
        return withContext(Dispatchers.IO){
            val response = retrofit.obtenerPokemones(200)
            response
        }
    }


}