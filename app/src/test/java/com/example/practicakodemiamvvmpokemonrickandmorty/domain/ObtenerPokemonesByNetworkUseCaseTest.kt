package com.example.practicakodemiamvvmpokemonrickandmorty.domain

import com.example.practicakodemiamvvmpokemonrickandmorty.data.models.Pokemon
import com.example.practicakodemiamvvmpokemonrickandmorty.data.models.PokemonResponse
import com.example.practicakodemiamvvmpokemonrickandmorty.repository.ServicePokemonNetwork
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import retrofit2.Response

class ObtenerPokemonesByNetworkUseCaseTest {

    @RelaxedMockK
    private lateinit var repository:ServicePokemonNetwork

    lateinit var casoDeUso : ObtenerPokemonesByNetworkUseCase

    @Before
    fun onBefore(){
        MockKAnnotations.init(this)
        casoDeUso = ObtenerPokemonesByNetworkUseCase(repository)
    }

    @Test
    fun invoke()= runBlocking {
        //Given
        val pokemones = ArrayList<Pokemon>()
        pokemones.add(Pokemon("prueba"))

        val response = PokemonResponse()
        response.pokemones = pokemones

        coEvery { repository.obtenerPokemones() } returns Response.success(response)
        //When
        val respuesta = casoDeUso.invoke().body()
        //Then

        //Verfiar si se ejecuta una funcion
      // coVerify { repository.obtenerPokemones() }
        assert(respuesta == Response.success(response.pokemones).body())
    }
}