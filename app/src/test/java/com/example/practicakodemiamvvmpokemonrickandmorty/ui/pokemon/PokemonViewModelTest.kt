package com.example.practicakodemiamvvmpokemonrickandmorty.ui.pokemon

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.practicakodemiamvvmpokemonrickandmorty.data.models.ErrorResponse
import com.example.practicakodemiamvvmpokemonrickandmorty.data.models.Pokemon
import com.example.practicakodemiamvvmpokemonrickandmorty.domain.ObtenerPokemonesByNetworkUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import junit.framework.TestCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Response

@ExperimentalCoroutinesApi
class PokemonViewModelTest {

    private lateinit var viewModel: PokemonViewModel


    @RelaxedMockK
    private lateinit var casoUso : ObtenerPokemonesByNetworkUseCase



    @get:Rule
    var schedulers :InstantTaskExecutorRule = InstantTaskExecutorRule()





    @Before
    fun onBefore(){
        //inicializar Mock
        MockKAnnotations.init(this)

        //Inicializamos el viewmodel
        viewModel = PokemonViewModel()

        //Configuracion dispacher
        Dispatchers.setMain(Dispatchers.Unconfined)

        //asignar caso de uso mockeado
        viewModel.useCase = casoUso

    }

    @After
    fun onAfter(){
        //Reset Dispacher
        Dispatchers.resetMain()
    }

    @Test
    fun checarPokemonesCasodeUso() = runTest {

        //variables
        val pokemones = ArrayList<Pokemon>()
        pokemones.add(Pokemon("Pokemon 1"))
        pokemones.add(Pokemon("Pokemon 2"))

        //configuracion
        coEvery { casoUso.invoke()}    returns Response.success(pokemones)

        //cuando se va a ejecutar
        viewModel.getPokemons()

        //respuesta
        assert(viewModel.pokemones.value == pokemones)

    }

    @Test
    fun checarError() = runTest {
        //variables
        val string ="{" +
                "protocol:\"h2\",\n" +
                " code:400,\n" +
                " message:\"error\", \n" +
                "url:\"https://rickandmortyapi.com/api/character\"\n" +
                "}"
        val response = ResponseBody.create(MediaType.parse("text/*"),string)

        //configuracion
        coEvery { casoUso.invoke() } returns Response.error(400,response)


        viewModel.getPokemons()

        val errorViewmodel  = viewModel.error.value as ErrorResponse


        assert(errorViewmodel.code == 400)
    }

    /*@Test
    fun getPokemons()= runTest {
      //  val pokemones = ArrayList(Pokemon("Pokemon 1"), Pokemon("Pokemon 2"))
        val pokemones = ArrayList<Pokemon>()
        pokemones.add(Pokemon("Pokemon 1"))
        pokemones.add(Pokemon("Pokemon 2"))

        coEvery { casoUso.invoke()} returns Response.success(pokemones)

        viewModel.getPokemons()

        assert(viewModel.pokemones.value == pokemones)
    }*/

}