package com.example.practicakodemiamvvmpokemonrickandmorty.data.models

import com.google.gson.annotations.SerializedName

data class ErrorResponse(@SerializedName("protocol" ) var protocol : String? = null,
                         @SerializedName("code"     ) var code     : Int?    = null,
                         @SerializedName("message"  ) var message  : String? = null,
                         @SerializedName("url"      ) var url      : String? = null

)
