package com.example.practicakodemiamvvmpokemonrickandmorty.ui.pokemon

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.practicakodemiamvvmpokemonrickandmorty.data.models.ErrorResponse
import com.example.practicakodemiamvvmpokemonrickandmorty.data.models.Pokemon
import com.example.practicakodemiamvvmpokemonrickandmorty.domain.ObtenerPokemonesByNetworkUseCase
import com.example.practicakodemiamvvmpokemonrickandmorty.repository.ServicePokemonNetwork
import com.google.gson.Gson
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.ArrayList

class PokemonViewModel : ViewModel() {

    //livedatas

    val pokemones = MutableLiveData<ArrayList<Pokemon>>()
    val error = MutableLiveData<ErrorResponse>()
    val cargando = MutableLiveData<Boolean>()

    private val service = ServicePokemonNetwork()

    //casos de uso
    var useCase = ObtenerPokemonesByNetworkUseCase(service)

    fun getPokemons(){

        viewModelScope.launch {
            cargando.postValue(true)
            val respuesta = useCase.invoke()
            try {
                if (respuesta.isSuccessful && respuesta.code() ==200){
                    pokemones.postValue(respuesta.body())
                }else if (respuesta.code() == 400){
                    val gson = Gson()
                    val errorResponse = gson.fromJson(respuesta.errorBody()!!.string(),ErrorResponse::class.java)
                    error.postValue(errorResponse)
                }
                cargando.postValue(false)
            }catch (io : IOException){
                error.postValue(io.message?.let { ErrorResponse(null, 400) })
                cargando.postValue(false)
               // error.postValue(io.message)
            }
        }
    }
}