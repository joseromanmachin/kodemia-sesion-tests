package com.example.practicakodemiamvvmpokemonrickandmorty.ui.pokemon

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.practicakodemiamvvmpokemonrickandmorty.R
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class PokemonFragmentTest {

  // @get:Rule var activityTestScenarioRule = ActivityScenarioRule<PokemonFragment>()


    @Test
    fun ingresoDatos(){

        val escenario = launchFragmentInContainer<PokemonFragment>()

        escenario.recreate()
        //accediendo ala vista
        onView(withId(R.id.editTextTextPersonName))

        //dar accion
            .perform(typeText("Este es un nombre ") , closeSoftKeyboard())

        //accediendo ala vista
        onView(withId(R.id.editTextNumberPassword))

            //dar accion
            .perform(typeText("1111") , closeSoftKeyboard())

        onView(withId(R.id.button)).perform(click())



    }
}