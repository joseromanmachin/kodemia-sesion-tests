package com.example.practicakodemiamvvmpokemonrickandmorty.domain

import com.example.practicakodemiamvvmpokemonrickandmorty.data.models.Pokemon
import com.example.practicakodemiamvvmpokemonrickandmorty.repository.ServicePokemonNetwork
import retrofit2.Response

class ObtenerPokemonesByNetworkUseCase(val service:ServicePokemonNetwork) {
   // val service = RepositorioPokemonNetwork()

    suspend fun invoke() : Response<ArrayList<Pokemon>>{
        val respuesta = service.obtenerPokemones().body()?.pokemones

        return Response.success(respuesta)
    }
}